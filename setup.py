from setuptools import setup
import os

from HyType import VERSION

with open(os.path.join(os.path.dirname(__file__), "README.md"), "r") as readme_file:
    long_description = readme_file.read()

setup(
    name = "HyType",
    version = VERSION,
    packages = ["HyType"],
    entry_points = {
        "console_scripts": [
            "hytype = HyType:hytype_cli"
        ]
    },
    license = "MIT",
    long_description = long_description,
)
