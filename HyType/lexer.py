from dataclasses import dataclass

import yaml


@dataclass
class Token:
    type: str
    content: str
    line: int
    column: int

    def __init__(self, type, content, line = 0, column = 0):
        self.type = type
        self.content = content
        self.line = line
        self.column = column

    def __repr__(self):
        return (f"Token(type: {self.type}, content: '{self.content}', line {self.line}, column {self.column})")


class Lexer:

    def __init__(self, string):

        self.lines = iter(string.splitlines())
        self.chars = None
        self.tokens = []

        self.current_line = None
        self.current_line_number = 0

        self.current_char = None
        self.current_char_number = 0


    def next_line(self):
        try:
            self.current_line = next(self.lines)
            self.current_line_number += 1
            self.chars = iter(self.current_line)
            return True

        except StopIteration:
            self.current_line = None
            return False


    def next_char(self):

        try:
            self.current_char = next(self.chars)
            self.current_char_number += 1
            return True

        except StopIteration:
            self.current_char = None
            return False


    def generate_tokens(self):

        while (self.next_line()):
            print(f"{self.current_line_number:2}: {self.current_line}")

            # ignore lines with only whitespace
            if not self.current_line.strip():
                continue

            if self.current_line == "---":
                self.tokens.append(
                    Token("meta", self.current_line, self.current_line_number))
                continue

            self.current_line.search("{*}")

            while (self.next_char()):
                #print(f"{self.current_char_number}: {self.current_char}")
                content = ""
                if self.current_char == "{":
                    self.tokens.append(
                        Token("\\", "\\", self.current_line_number, self.current_char_number)
                    )
                    continue

        for token in self.tokens:
            print(token)


    def check_meta(self):
        if self.meta:
            return False

        if self.current_line == "---":
            if self.meta_open:
                self.meta = True
                self.tokens.append(("meta close", "---"))
                return True

            self.meta_open = True
            self.tokens.append(("meta open", "---"))
            return True

        if self.meta_open:
            self.tokens.append(("meta", self.current_line))
            return True


    def lex(self):

        while(True):
            try:
                self.current_line = next(self.lines)
                self.current_line_number += 1
            except StopIteration:
                self.current_line = None
                break

            print(f"{self.current_line_number}: {self.current_line.strip()}")

            if self.check_meta():
                continue

            self.tokens.append(("markdown", self.current_line))

            #self.parse_line(self.current_line)

        for token in self.tokens:
            print(token)



def main():
    string = """
---
data1: asdf
data2: qwert
---

{titlepage}

Line 1
line 2
line 3
"""

    print("MetaDataReader")
    meta_data_reader = MetaDataReader(string)
    meta_data_reader.get_meta_data()

    print("Lexer")
    lexer = Lexer(string)
    lexer.generate_tokens()

if __name__ == "__main__":
    main()
