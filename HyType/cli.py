"""
    HyType.cli
    ==============

    Command-line interface to HyType.
"""


import click
import os

from .project import VERSION
from .parser import parse_file, parse_markdown
from .processor import process_file, process_html
from .printer import print_file_to_pdf, print_html_to_pdf


@click.group()
def hytype_cli(auto_envvar_prefix='HYTYPE'):
    pass


@hytype_cli.command()
def version():
    print("HyType version {}".format(VERSION))


@hytype_cli.command()
@click.argument('input', type = click.File('r'))
@click.argument('output', type = click.File('w'))
def parse(input,output):
    parse_file(input, output)


@hytype_cli.command()
@click.argument('input', type = click.Path())
@click.argument('output', type = click.File('w'))
def process(input, output):
    process_file(input, output)


@hytype_cli.command()
@click.argument('input', type = click.Path())
@click.argument('output', type = click.Path())
def print_pdf(input, output):
    print_file_to_pdf(input, output)


@hytype_cli.command()
@click.argument('input', type = click.Path())
@click.argument('output', type = click.Path())
def hytype(input, output):

    # get base url to resolve html links
    base_url = os.path.dirname(os.path.abspath(input))

    # read markdown input from file
    with open(input) as file:
        markdown = file.read()

    # process document
    html = parse_markdown(markdown)
    html = process_html(html, base_url)
    print_html_to_pdf(html, output, base_url = base_url)
