#!/usr/bin/env python3

"""
    HyType.__main__
    ===============

    The main function of HyType offers a command-line interface to HyType.
"""

from .cli import hytype_cli

if __name__ == '__main__':
    hytype_cli()
