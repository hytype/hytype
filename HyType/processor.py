"""
    HyType.processor
    ================
"""


import os
from pyquery import PyQuery as pq
import requests
from urllib.parse import urljoin, urlparse


def process_html(html, base_url):
    # Load HTML into PyQuery object
    doc = pq(html)

    # Add head to document if not existent
    if not doc('head'):
        doc('html').prepend('<head/>')
    head = doc('head')

    # load linked scripts and include them into document
    for script in doc('script[src]'):
        src_url = urljoin('file://' + base_url + '/', script.attrib['src'])
        src_url_parsed = urlparse(src_url)
        if src_url_parsed.scheme == 'file':
            with open(src_url_parsed.path,"r") as script_file:
                script_content = script_file.read()
        else:
            raise NotImplementedError("remote URL schemes for script parsing not implemented")
        del script.attrib['src']
        script.text = script_content

    # load linked stylesheets and include them into document
    for link_element in doc("link[rel='stylesheet']"):
        href_url = urljoin('file://' + base_url + '/', link_element.attrib['href'])
        href_url_parsed = urlparse(href_url)
        if href_url_parsed.scheme == 'file':
            with open(href_url_parsed.path,"r") as stylesheet_file:
                stylesheet_content = stylesheet_file.read()
        else:
            raise NotImplementedError("remote URL schemes for script parsing not implemented")
        stylesheet = pq('<style/>')
        stylesheet.text(stylesheet_content)
        head.append(stylesheet)
    doc.remove("link[rel='stylesheet']")

    # return processed HTML string
    output_html = "<!DOCTYPE html>\n"
    output_html += str(doc)
    return output_html


def process_file(input, output):
    # get base url to resolve html links
    base_url = os.path.dirname(os.path.abspath(input))

    # read html input from file
    with open(input) as file:
        html_input = file.read()

    # process HTML input from file
    html_output = process_html(html_input, base_url)

    # write processed html to file
    output.write(html_output)
