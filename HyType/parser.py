"""
    HyType.parser
    =============

    The HyType parser translates markdown input into .
"""


import marko

from .metadata_reader import MetadataReader


def parse_markdown(markdown):
    """Takes markdown string and returns translated HTML string."""

    # read metadata string at beginning of document
    metadata = None
    metadata_reader = MetadataReader(markdown)
    if metadata_reader.search_metadata() and metadata_reader.parse_metadata():
        metadata = metadata_reader.get_metadata()
        markdown = metadata_reader.get_content()

    html = "<!DOCTYPE html>\n<html>\n"

    if metadata is not None:
        html += "<head>\n"

        if "title" in metadata:
            html += f'<title>{metadata["title"]}</title>\n'

        if "style" in metadata:
            html += f'<link rel="stylesheet" href="{metadata["style"]}">\n'

        html += "</head>\n"

    html += "<body>\n"
    html += marko.convert(markdown)
    html += "</body>\n</html>\n"
    return html


def parse_file(input, output):
    """
    Reads markdown from input file and writes translated HTML to output file.
    """

    markdown = input.read()
    html = parse_markdown(markdown)
    output.write(html)
