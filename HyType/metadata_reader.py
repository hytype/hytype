import yaml


class MetadataReader:
    """Reder for meta data in yaml format at beginning of file"""

    def __init__(self, string):
        self.lines = string.splitlines()
        self.lines_iter = iter(self.lines)

        self.metadata_lines = []
        self.metadata = None

        self.meta_tag_open = False
        self.metadata_found = False

        self.current_line = None
        self.current_line_number = 0

        self.open_tag_line_number = 0
        self.close_tag_line_number = 0


    def next_line(self):
        """Advances reader to the next line"""
        try:
            self.current_line = next(self.lines_iter)
            self.current_line_number += 1
            return True

        except StopIteration:
            self.current_line = None
            if self.meta_tag_open:
                print(f"Error: Meta data section was opened in line {self.open_tag_line_number} but never closed.")
                print(f"{self.open_tag_line_number}: '---'\n")
            return False


    def check_meta_tag(self):
        """Check if current line is opening or closing meta tag."""

        # not a meta tag
        if not self.current_line == "---":
            return False

        # end of meta string reached
        if self.meta_tag_open:
            self.metadata_found = True
            self.close_tag_line_number = self.current_line_number
            return True

        # beginning of meta string found
        else:
            self.meta_tag_open = True
            self.open_tag_line_number = self.current_line_number
            return True


    def search_metadata(self):
        """Searches for yaml meta data at beginning of document."""

        while(self.next_line() and not self.metadata_found):

            # ignore lines with only whitespace
            if not self.current_line.strip():
                continue

            # check for opening or closing meta tag
            if self.check_meta_tag():
                continue

            # read line tagged as meta data
            if self.meta_tag_open:
                self.metadata_lines.append(self.current_line)

            # contnet found before opening meta tag
            else:
                print("Info: No meta tag found at beginning of file")
                return False

        # reached end of file
        return self.metadata_found


    def parse_metadata(self):
        """Parses yaml."""

        # check if meta data string was found
        if not self.metadata_found:
            print("Error: No meta data string was found at beginning of document.")
            return False

        # parse yaml meta data string
        metadata_string = "\n".join(self.metadata_lines)
        try:
            self.metadata = yaml.load(metadata_string, Loader = yaml.Loader)
            return True

        # handle yaml error
        except yaml.scanner.ScannerError as e:
            print("Error: A yaml error occurred while parsing meta data string, "\
                f"lines {self.open_tag_line_number} to {self.close_tag_line_number}:")

            # print meta data string
            print("     ---")
            for i in range(len(self.metadata_lines)):
                print(f"{i+1:3}: {self.metadata_lines[i]}")
            print("     ---")

            # print yaml error message
            print(e)

            return False


    def get_metadata(self):
        """Returns metadata as dict after parsing or None if not available."""

        if (self.metadata is None):
            print("Error: No metadata available.")

        return self.metadata


    def get_content(self):
        """Returns the content of the input string with metadata string removed after search is completed."""

        if self.metadata_found:
            return "\n".join(self.lines[self.close_tag_line_number:])

        else:
            return "\n".join(self.lines)


def main():
    string = """
---
data1: asdf
data2: qwert
---

{titlepage}

Line 1
line 2
line 3
"""

    metadata_reader = MetadataReader(string)

    if metadata_reader.search_metadata() and metadata_reader.parse_metadata():
        print("Metadata:")
        print(metadata_reader.get_metadata())
        print("Content:")
        print(metadata_reader.get_content())


if __name__ == "__main__":
    main()
