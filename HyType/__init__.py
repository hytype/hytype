"""
    HyType
    ======

    HyType is a hypertext typesetting system.
"""

from .cli import hytype_cli

from .project import VERSION
__version__ = VERSION
