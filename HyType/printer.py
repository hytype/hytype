"""
    HyType.printer
    ==============

    The HyType printer takes HTML input and renders a output file.

    At the moment the printer relies to weasyprint.

    Supported output formats are:
    - PDF
"""


import os
import logging

from weasyprint import HTML


def init_weasyprint_log(dir):
    """Initialize weasyprint logger and write log file to given directory."""

    logger = logging.getLogger('weasyprint')
    logger.addHandler(
        logging.FileHandler(os.path.join(dir, 'weasyprint.log')))


def print_html_to_pdf (html, output, base_url = None, weasyprint_log = True):
    """Takes HTML input and prints PDF output."""

    # init weasyprint logger
    if weasyprint_log:
        # write log to output directory
        out_dir = os.path.dirname(os.path.abspath(output))
        init_weasyprint_log(out_dir)

    # render pdf
    if (base_url is None):
        pdf = HTML(string = html)
    else:
        pdf = HTML(string = html, base_url = base_url)
    pdf.write_pdf(output)


def print_file_to_pdf (input, output):
    """Takes path to HTML file as input and prints PDF to output path."""

    # get base url to resolve html links
    base_url = os.path.dirname(os.path.abspath(input))

    # read html input from file
    with open(input) as file:
        html = file.read()

    # render pdf using weasyprint
    print_html_to_pdf(html, output, base_url = base_url, weasyprint_log = True)
